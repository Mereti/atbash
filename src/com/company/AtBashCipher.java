package com.company;

import java.util.ArrayList;
import java.util.List;

class AtBashCipher implements Cipher {

    private static final int GROUP_SIZE = 3;
    private static final String ALFAONE = "abcdefghijklmnopqrstuvwxyz";
    private static final String REVERSE = "zyxwvutsrqponmlkjihgfedcba";

    @Override
    public String decode(String message) {

        String encoded = stripInvalidCharacters(message).toLowerCase();
        String finish = "";

        for (char i : encoded.toCharArray()) {
            finish += applyCipher(i);
        }

        return finish;
    }

    @Override
    public String encode(String message) {
        String encoded = stripInvalidCharacters(message).toLowerCase();
        String finish = "";

        for (char c : encoded.toCharArray()) {
            finish += applyCipher(c);
        }
        return splitIntoLetterWords(finish);
    }
    private String stripInvalidCharacters(String input) {
        String filteredValue = "";

        for (char i : input.toCharArray()) {
            if (Character.isLetterOrDigit(i)) {
                filteredValue += i;
            }
        }

        return filteredValue;
    }

    private char applyCipher(char message) {
        int indexOfLetter = ALFAONE.indexOf(message);

        return indexOfLetter >= 0 ? REVERSE.toCharArray()[indexOfLetter] : message;
    }

    private String splitIntoLetterWords(String value) {
        List<String> letters = new ArrayList<>();

        for (int i = 0; i < value.length(); i += GROUP_SIZE) {
            letters.add(i + GROUP_SIZE <= value.length() ? value.substring(i, i + GROUP_SIZE) : value.substring(i));
        }

        return String.join(" ", letters);
    }
}
